package com.greysonparrelli.portduffbug;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class CornerMaskingView extends FrameLayout {

  private final float[] radii      = new float[8];
  private final Paint   dstPaint   = new Paint();
  private final Paint   clearPaint = new Paint();
  private final Path    outline    = new Path();
  private final Path    corners    = new Path();
  private final RectF   bounds     = new RectF();

  public CornerMaskingView(@NonNull Context context) {
    super(context);
    init();
  }

  public CornerMaskingView(@NonNull Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    init();
  }

  public CornerMaskingView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }

  private void init() {
    setLayerType(LAYER_TYPE_HARDWARE, null);

    dstPaint.setColor(Color.BLACK);
    dstPaint.setStyle(Paint.Style.FILL);
    dstPaint.setAntiAlias(true);
    dstPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));

    clearPaint.setColor(Color.BLACK);
    clearPaint.setStyle(Paint.Style.FILL);
    clearPaint.setAntiAlias(true);
    clearPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
  }

  @Override
  protected void dispatchDraw(Canvas canvas) {
    super.dispatchDraw(canvas);

    bounds.left   = 0;
    bounds.top    = 0;
    bounds.right  = canvas.getWidth();
    bounds.bottom = canvas.getHeight();

    corners.reset();
    corners.addRoundRect(bounds, radii, Path.Direction.CW);

//    workingAndroidP(canvas);
    notWorkingAndroidP(canvas);
  }

  private void notWorkingAndroidP(Canvas canvas) {
    // This should work on Android P, but doesn't. It works on every other Android version tested.
    corners.addRoundRect(bounds, radii, Path.Direction.CW);
    canvas.drawPath(corners, dstPaint);
  }

  private void workingAndroidP(Canvas canvas) {
    // This works on all tested Android versions, but requires API 19, as well as additional math
    // on every draw.
    outline.reset();
    outline.addRect(bounds, Path.Direction.CW);
    outline.op(corners, Path.Op.DIFFERENCE);
    canvas.drawPath(outline, clearPaint);
  }

  public void setRadius(int radius) {
    setRadii(radius, radius, radius, radius);
  }

  public void setRadii(int topLeft, int topRight, int bottomRight, int bottomLeft) {
    radii[0] = radii[1] = topLeft;
    radii[2] = radii[3] = topRight;
    radii[4] = radii[5] = bottomRight;
    radii[6] = radii[7] = bottomLeft;
    invalidate();
  }
}
